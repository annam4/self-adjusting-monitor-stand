* [1/17/22](#2022-01-17 - Initial Brainstorming Session)
* [1/21/22](#2022-01-21 - New Idea & RFA Submission)
* [1/28/22](#2022-01-28 - Machine Shop)
* [2/5/22](#2022-02-05 - Block Diagram)
* [2/8/22](#2022-02-08 - First TA Meeting)
* [2/9/22](#2022-02-09 - Block Diagram 2)
* [2/15/22](#2022-02-15 - Confirmed Motors with Gregg)
* [2/20/22](#2022-02-20 - Schematic and Calculations)
* [2/24/22](#2022-02-24 - Software Research)
* [2/26/22](#2022-02-26 - Started Software)
* [2/28/22](#2022-02-28 - PCB Layout)
* [3/3/22](#2022-03-03 - Software Flowchart)
* [3/4/22](#2022-03-04 - Facial Detection)
* [3/7/22](#2022-03-07 - Convert to Physical Dimensions)
* [3/21/22](#2022-03-21 - Convert to Physical Dimensions 2)
* [4/5/22](#2022-04-05 - Running on Odroid)
* [4/6/22](#2022-04-06 - GPIO and WiringPi on Odroid)
* [4/7/22](#2022-04-07 - GPIO and WiringPi on Odroid 2)
* [4/8/22](#2022-04-08 - GPIO and WiringPi on Odroid 3)
* [4/17/22](#2022-04-17 - Begin Switch to Raspberry Pi)
* [4/19/22](#2022-04-19 - Running on Raspberry Pi)
* [4/20/22](#2022-04-20 - I2C Integration)
* [4/21/22](#2022-04-21 - I2C Integration 2)
* [4/22/22](#2022-04-22 - Mock Demo)
* [4/23/22](#2022-04-23 - I2C Integration 3)
* [4/24/22](#2022-04-24 - Switch to SPI)
* [4/25/22](#2022-04-25 - Finishing Touches)
* [4/26/22](#2022-04-26 - Demo)

<a name="2022-01-17 - Initial Brainstorming Session"></a>
# 2022-01-17 - Initial Brainstorming Session
Met to discuss some potential project ideas. 
<br>Top Ideas:
* automatic color-matcher/paint mixer
* auto "barista"
* water sensors
* phone accessory printer
* auto-locking bike lock
* pill recognizer and sorter

<a name="2022-01-21 - New Idea & RFA Submission"></a>
# 2022-01-21 - New Idea & RFA Submission
Came up with a new idea after some discussions with TA about issues with current project idea. Decided to propose a self-adjusting monitor stand. Submitted RFA.

The basic idea is to develop a self-adjusting monitor stand that will allow the user to setup his/her desk in a better, more ergonomic manner, with an optimal viewing angle. This will be accomplished using a face detection algorithm and a motorized stand with three degrees of freedom. We will allow the user to manually adjust the height of the monitor. To create the best viewing angle, we will use a "pan" motor and a "tilt" motor. These will rotate the monitor about the vertical axis and the horizontal axis respectively.

<a name="2022-01-28 - Machine Shop"></a>
# 2022-01-28 - Machine Shop
Visited the Machine Shop and talked with Gregg about our project idea. He suggested a rotating platform for panning, a motor for tilting, and a linear actuator for vertical adjustment. He also advised using the strongest motor we could and to update him on our motor choice as soon as we decided.

<a name="2022-02-05 - Block Diagram"></a>
# 2022-02-05 - Block Diagram
**1. Design Update**
<br>Began a first draft of our block diagram.
<br>Rough Sketch:
![Image](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/BlockDiagram_1st_Sketch.jpg)

**2. Part Update**
<br> Decided on a microcontroller for the processing and motor control subsystem: STM32GO61C8T6. <br> Ordered 3.

<a name="2022-02-08 - First TA Meeting"></a>
# 2022-02-08 - First TA Meeting
Met with Jamie for an initial discussion on proposal and overall project. Based on feedback, we updated the requirements in our proposal. 

**Part Update**
<br> We will need to choose a MOSFET H bridge driver, a regulator, motors, and a camera before Thursday. Decided to use Odroid XU4 as the CV processor.

<a name="2022-02-09 - Block Diagram 2"></a>
# 2022-02-09 - Block Diagram 2
Finished a formal version of the block diagram for our proposal. This version contains major part numbers and a better legend for the various voltage levels and communication protocols.
<br> ![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/Block_Diagram_ver1.png)

<a name="2022-02-15 - Confirmed Motors with Gregg"></a>
# 2022-02-20 - Confirmed Motors with Gregg
The past week was spent working on the proposal and finding suitable components, especially the motors. Our project required that the motors be operational at 12 V, move at a reasonable speed as to avoid damaging the monitor or injuring the user, and have the ability to support the load of the monitor while maintaining the necessary range of motion. An encoder was also necessary. Without one, we would not be able to accurately track how far the motor has moved. Ultimately we chose the following motors for the pan/tilt motors and the vertical motor:

Pan/Tilt Motors: <a href="https://www.pololu.com/product/4869" target="_blank">227:1 Metal Gearmotor 25Dx71L mm MP 12V with 48 CPR Encoder</a>

Vertical Motor: <a href="https://www.amazon.com/ECO-LLC-Acutator-Electric-Actuator/dp/B08HQRNGYM" target="_blank">ECO LLC L11TGF1000NB150HW-T-1</a>

Gregg approved these motors the same day, and we will be moving forward with our design.

<a name="2022-02-20 - Schematic and Calculations"></a>
# 2022-02-20 - Schematic and Calculations
Used EAGLE to create a circuit schematic and start the PCB layout. Worked with teammates to finalize any remaining components and footprints. 
For our motor drivers, we created an H-bridge using two half-bridge configurations for each motor. A bootstrap circuit was also required to ensure the high-side MOSFET is sufficiently biased to turn on at the voltage levels required in our design. The maximum duty cycle we can run our motors at is 90% and we plan to switch at 20 kHz. Based on these values and the datasheet for our motor driver, the following resistor and capacitor values were calculated:

**Bootstrap Capacitor (C_boot)**
<br> The capacitance must be greater than ten times the gate capacitor: C_g = Q_g/(V_dd - V_diode) = 11.3 nC/(12 V - 0.63 V) = 0.993 nF where Q_g is the gate charge of the MOSFET in the half-bridge, V_dd is the source voltage, and V_diode is the voltage across the bootstrap diode. Since the bootstrap must be at least ten times the gate capacitor, we selected a 10 nF capacitor. 

**Bootstrap Resistor (R_boot)** 
<br> The bootstrap resistor was determined by considering the peak current (I_pk = (V_dd - V_diode)/R_boot) and the time constant. For our desired switching speed, we determined that the RC constant cannot exceed 5 microseconds. Using the equation tau = (R_boot x C_boot)/(duty cycle) where tau is the time constant: 5 us > (R_boot x C_boot)/(duty cycle) = (R_boot x 10 nF)/0.9. Based on this calculation, the maximum resistor value is 450 ohms. We selected a 330 ohm resistor.

**HIN/LIN Values**
<br> The motor driver data sheet provided the optimal values. The capacitors are 120 pF, and the resistors are 100 ohms.

**Gate Resistor**
<br> For the gate resistor, we selected a value 1000 ohms.

<br>Schematic:
![Image](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/eagle_Schematic.png)

Gregg also responded to our request for a simple sketch of his plan for our monitor stand:
![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/greggSketch.png)

References:

1. Mamadou Diallo, “Bootstrap Circuitry Selection for Half-Bridge Configurations,” Texas Instruments Incorporated, 2018. [Online]. Available: https://www.ti.com/lit/an/slua887/slua887.pdf?ts=1645368266361.

2. “Driving a high current DC motor using an H-bridge,” Driving a high current DC Motor using an H-bridge - Northwestern Mechatronics Wiki, 03-Nov-2009. [Online]. Available: http://hades.mech.northwestern.edu/index.php/Driving_a_high_current_DC_Motor_using_an_H-bridge.

<a name="2022-02-24 - Software Research"></a>
# 2022-02-24 - Software Research
Began preparation to begin writing the code for facial detection. We chose to use Haar feature-based cascade classifiers.
This method of object detection uses positive and negative images to train the classifier. Facial features such as eyes or the bridge of the nose can then be extracted using Haar Edge, Line, and Four-rectangle features:
<br>![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/haar_features.jpg)

Each feature is a value calculated by subtracting the sum of pixels under the white rectangle from the sum of pixels under the black rectangle. An integral image is used to speed up this calculation. The best featuresare selected using Adaboost, which iteratively determines the best classifier with wn acceptable error rate.

A cascade of classifiers is also used to improve the efficiency of detection. THe features are grouped into different stages of classifiers and applied and applied one at a time. If a window fails a stage, it is discarded and the remaining features are ignored. A window that passes all stages should represent a face region.

For our code, we will take advantage of Haar cascade data provided by OpenCV in their documentation. OpenCV has datasets with sufficient positive and negative images for faces that can be used to train our classifier. The next steps will be to write code that will detect a person's face, implement some method to ensure that the user's face is used in positioning and not a face in the background, and convert the visual data to physical dimensions that can be used to calculate how much our motors should rotate so that the monitor is centered on the user's face.

References:
1. P. Viola and M. Jones, "Rapid object detection using a boosted cascade of simple features," Proceedings of the 2001 IEEE Computer Society Conference on Computer Vision and Pattern Recognition. CVPR 2001, 2001, pp. I-I, doi: 10.1109/CVPR.2001.990517.

2. “Cascade classifier,” OpenCV. [Online]. Available: https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html. 

<a name="2022-02-26 - Started Software"></a>
# 2022-02-26 - Started Software
Iris and I began writing the code that would be used for face detection. This first version was written based on the provided code in the OpenCV documentation. After installing the necessary pacakges, we ran the code to see how it worked and to understand each line. This early version simply analyzed a frame provided by live video, circled an faces and eyes it detected, and displayed the result. Unfortunately, we will not be able to display the live video witht he face tracked. Doing so caused the code to crash when more functions were added.
```
from __future__ import print_function
import cv2 as cv
import argparse

def detectAndDisplay(frame):
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_gray = cv.equalizeHist(frame_gray)
    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    for (x,y,w,h) in faces:
        center = (x + w//2, y + h//2)
        frame = cv.ellipse(frame, center, (w//2, h//2), 0, 0, 360, (255, 0, 255), 4)
        faceROI = frame_gray[y:y+h,x:x+w]
        #-- In each face, detect eyes
        eyes = eyes_cascade.detectMultiScale(faceROI)
        for (x2,y2,w2,h2) in eyes:
            eye_center = (x + x2 + w2//2, y + y2 + h2//2)
            radius = int(round((w2 + h2)*0.25))
            frame = cv.circle(frame, eye_center, radius, (255, 0, 0 ), 4)
    cv.imshow('Capture - Face detection', frame)
parser = argparse.ArgumentParser(description='Code for Cascade Classifier tutorial.')
parser.add_argument('--face_cascade', help='Path to face cascade.', default='data/haarcascades/haarcascade_frontalface_alt.xml')
parser.add_argument('--eyes_cascade', help='Path to eyes cascade.', default='data/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
parser.add_argument('--camera', help='Camera divide number.', type=int, default=0)
args = parser.parse_args()
face_cascade_name = args.face_cascade
eyes_cascade_name = args.eyes_cascade
face_cascade = cv.CascadeClassifier()
eyes_cascade = cv.CascadeClassifier()
#-- 1. Load the cascades
if not face_cascade.load(cv.samples.findFile(face_cascade_name)):
    print('--(!)Error loading face cascade')
    exit(0)
if not eyes_cascade.load(cv.samples.findFile(eyes_cascade_name)):
    print('--(!)Error loading eyes cascade')
    exit(0)
    

#CHANGE THIS FOR ODROID
#camera_device = args.camera
camera_device = 0 #just webcam

#-- 2. Read the video stream
cap = cv.VideoCapture(camera_device)
if not cap.isOpened:
    print('--(!)Error opening video capture')
    exit(0)
while True:
    ret, frame = cap.read()
    if frame is None:
        print('--(!) No captured frame -- Break!')
        break
    detectAndDisplay(frame)
    if cv.waitKey(10) == 27:
        break
```

<a name="2022-02-28 - PCB Layout"></a>
# 2022-02-28 - PCB Layout
Met up to complete the PCB layout in time for the board review. Assisted Jake in the layout of the motor drivers and H-bridge circuits. The motor driver datasheets had specific layout recommendations for the capacitors and resistors, which we double checked.

Additionally, Iris made a final draft of our block diagram based on feedback from our design review. The changes included some clearer color differences, minor fixes, and a differentiation between 12 VDC and the PWM 12 V.
![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/blockDiagram_FINAL.png)

<a name="2022-03-03 - Software Flowchart"></a>
# 2022-03-03 - Software Flowchart
Developed a flowchart for the face detection code. This will be used to finish writing the code.
<br>![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/facedetection_flowchart.png)
<br>First, the cascade data is loaded and the USB camera is accessed. Then, the frame is checked for a face. The frame will be continually checked until at least one face is found. Next, each face analyzed and a basic area calculation is performed. Each area is compared, and the face with the largest area is used for the rest of the code. An image of the current frame is then saved locally and used to calculate the offset between the center of the user's face and the center of the camera frame. The data is then sent to the microcontroller using I2C, and the local image is finally deleted. This process is placed inside a while loop so that the code runs continuously.

<a name="2022-03-04 - Facial Detection"></a>
# 2022-03-04 - Facial Detection
Finished writing the first iteration of our facial detection code. Following the flowchart developed earlier, the code currently determines the offset position between the center of the frame and the user's face. The next step is to convert the offset in pixels to a degree offset that can be used to move the motors.
```
from __future__ import print_function
import cv2 as cv
import argparse
import numpy  as np
import time
import os
from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
import imutils

def detectFace(frame):
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_gray = cv.equalizeHist(frame_gray)

    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    maxRect = 10000     # define the minimum area for a detected face
    #print("check")
    face = 0
    # If a face is detected, save the image
    if len(faces) > 0:
        img_name = "opencv_frame.png"
        cv.imwrite(img_name, frame)
        #print("{} written!".format(img_name))
        #print("face found!")
        face = 1

    return face

def analyzeFrame(imagePath):
    midpoint = [0,0] # midpoint between eyes
    eye_cnt = 0 # keep track of how many eyes
    frame = cv.imread(imagePath)
    # print("image")
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # cv.imshow('Gray image', frame_gray)
    frame_gray = cv.equalizeHist(frame_gray)

    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    maxRect = 0     # define the minimum area for a detected face
#    print('Faces: ' + str(len(faces)))
    for i in range(len(faces)):
        (x,y,w,h)=faces[i]
        area = np.pi*(faces[i][2]*faces[i][3])/4
        # work with the "biggest" face in the frame to avoid adjusting to a background person
        if area > maxRect:
            maxRect = area
            center = (x + w//2, y + h//2)
            frame = cv.ellipse(frame, center, (w//2, h//2), 0, 0, 360, (0, 255, 0), 4)
            faceROI = frame_gray[y:y+h,x:x+w]
            #-- In each face, detect eyes
            eyes = eyes_cascade.detectMultiScale(faceROI)
            for j in range(len(eyes)):
                (x2,y2,w2,h2) = eyes[j]
                # print("eye")
                eye_cnt += 1
                eye_center = [x + x2 + w2//2, y + y2 + h2//2]
                midpoint[0] += eye_center[0]
                midpoint[1] += eye_center[1]
                # print(eye_center)
                # print(midpoint)
                radius = int(round((w2 + h2)*0.25))
                frame = cv.circle(frame, eye_center, radius, (255, 0, 0 ), 4)
        #    print('Eyes: ' + str(eye_cnt))
            if(eye_cnt == 2):
                target = (midpoint[0]//2, midpoint[1]//2)
            elif(eye_cnt == 1):
                target = (x + w//2, midpoint[1])
            else:
                target = (x + w//2, y + h//2 - h//4)
            # Draw a circle between the eyes
            #frame = cv.circle(frame, target, 10, (255, 0, 255 ), 4)
            # print(target)
    # Draw a circle where a point between the eyes should fall in 
    #goal = cv.ellipse(frame, (310,240), (10, 10), 0, 0, 360, (0, 0, 255), 4)
    # FOR TESTING ONLY:
    img_name = "opencv_detected.png"
    cv.imwrite(img_name, frame)
    # print("done!")
    offset = [310-target[0], 240-target[1]]
    print('Offset from goal position: ', offset)
    return offset

parser = argparse.ArgumentParser(description='Code for Cascade Classifier tutorial.')
parser.add_argument('--face_cascade', help='Path to face cascade.', default='data/haarcascades/haarcascade_frontalface_alt.xml')
parser.add_argument('--eyes_cascade', help='Path to eyes cascade.', default='data/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
parser.add_argument('--camera', help='Camera divide number.', type=int, default=0)
args = parser.parse_args()
face_cascade_name = args.face_cascade
eyes_cascade_name = args.eyes_cascade
face_cascade = cv.CascadeClassifier()
eyes_cascade = cv.CascadeClassifier()

#-- 1. Load the cascades
if not face_cascade.load(cv.samples.findFile(face_cascade_name)):
    print('--(!)Error loading face cascade')
    exit(0)
if not eyes_cascade.load(cv.samples.findFile(eyes_cascade_name)):
    print('--(!)Error loading eyes cascade')
    exit(0)
    
#-- 2. Read the video stream
face = 0    # flag to indicate if face was detected
offset = (100,100)
cap = cv.VideoCapture(0, cv.CAP_DSHOW)
# OPTION 1
# Code for finding the face and calculating offset
# if not cap.isOpened:
#     print('--(!)Error opening video capture')
#     exit(0)
# while face == 0:
#     ret, frame = cap.read()
#     if frame is None:
#         print('--(!) No captured frame -- Break!')
#         break
#     face = detectFace(frame)
# cap.release()   # stop recording

# OPTION 2
### TESTING: continue finding face and offset until the face is within acceptable bounds fo the goal ###
while (offset[0] > 10 or offset[0] < -10) or (offset[1] > 10 or offset[1] < -10):
    if not cap.isOpened:
        print('--(!)Error opening video capture')
        exit(0)
    while face == 0:
        ret, frame = cap.read()
        if frame is None:
            print('--(!) No captured frame -- Break!')
            break
        face = detectFace(frame)
    cap.release()   # stop recording
########################################################################################################
    # unindent the code block below later
    #-- 3. Analyze the saved image
    path = r'opencv_frame.png'    # CHANGE FOR ODROID
    # Determine the difference between face and the target position
    offset = analyzeFrame(path)
    if (offset[0] < 10 or offset[0] > -10) or (offset[1] > 10 or offset[1] < -10):
        print('nope')
        face = 0
        cap = cv.VideoCapture(0, cv.CAP_DSHOW)
    # Convert digital offset to motor information
print('good', offset)

# Delete image when finished
#os.remove(path)
```

<a name="2022-03-07 - Convert to Physical Dimensions"></a>
# 2022-03-07 - Convert to Physical Dimensions
Researched reliable methods for converting the pixel offsets in the image to a value in degrees. Found a few potential methods online and from other courses.

1. Use a projection matrix and the focal length of the camera to determine the scaling of the image to reality. This method is useful for object detection in 3D space, and would provide an accurate reading. Unfortunately, this method would also entail more complex computation and further research into how developing the math into a robust code. 
2. Calibrate the image using some checkerboard or circle grid. Will need to research further into this method. There appears to be some existing code and documentation for using calibration to determine the physical dimensions from the camera image.
3. Determine the physical dimensions of the image by finding some "pixels per metric" ratio. This seems to be the simplest method, although less robust. To use this method, we need the focal length of the camera (3.85 m) and the average width of a human face (142.5 mm). This will be used to determine the distance of the face from the screen and the ratio of physical width to pixel width.

References:
1. “Camera calibration using a circle grid,” Longer Vision Technology, 06-Jun-2021. [Online]. Available: https://longervision.github.io/2017/03/18/ComputerVision/OpenCV/opencv-internal-calibration-circle-grid/.
2. G. Steve, et. al., “Cascade classifier,” OpenCV. [Online]. Available: https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html.

<a name="2022-03-21 - Convert to Physical Dimensions 2"></a>
# 2022-03-21 - Convert to Physical Dimensions 2
Wrote the code for determining the rotation of the motors in radians. The focal length of the camera and average width of a face were used to determine an "effective focal length" based on an average value at distances similar to that of a user from a monitor screen. The implementation of this is shown in the code below, and the specific function for determining the ratio of width to pixel width is shown in pixToDim.
```
from __future__ import print_function
import cv2 as cv
import argparse
import numpy  as np
import time
import os
from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
import imutils

def detectFace(frame):
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_gray = cv.equalizeHist(frame_gray)

    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    maxRect = 10000     # define the minimum area for a detected face
    #print("check")
    face = 0
    # If a face is detected, save the image
    if len(faces) > 0:
        img_name = "opencv_frame.png"
        cv.imwrite(img_name, frame)
        #print("{} written!".format(img_name))
        #print("face found!")
        face = 1

    return face

def analyzeFrame(imagePath):
    midpoint = [0,0] # midpoint between eyes
    eye_cnt = 0 # keep track of how many eyes
    frame = cv.imread(imagePath)
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_gray = cv.equalizeHist(frame_gray)

    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    maxRect = 0     # define the minimum area for a detected face
    for i in range(len(faces)):
        (x,y,w,h)=faces[i]
        area = np.pi*(faces[i][2]*faces[i][3])/4
        # work with the "biggest" face in the frame to avoid adjusting to a background person
        if area > maxRect:
            maxRect = area
            center = (x + w//2, y + h//2)
            frame = cv.ellipse(frame, center, (w//2, h//2), 0, 0, 360, (0, 255, 0), 4)
            faceROI = frame_gray[y:y+h,x:x+w]
            #-- In each face, detect eyes
            eyes = eyes_cascade.detectMultiScale(faceROI)
            for j in range(len(eyes)):
                (x2,y2,w2,h2) = eyes[j]
                eye_cnt += 1
                eye_center = [x + x2 + w2//2, y + y2 + h2//2]
                midpoint[0] += eye_center[0]
                midpoint[1] += eye_center[1]
                radius = int(round((w2 + h2)*0.25))
                frame = cv.circle(frame, eye_center, radius, (255, 0, 0 ), 4)
            if(eye_cnt == 2):
                target = (midpoint[0]//2, midpoint[1]//2)
            elif(eye_cnt == 1):
                target = (x + w//2, midpoint[1])
            else:
                target = (x + w//2, y + h//2 - h//4)
        d, pixeld = pixToDim(w)
        print("d: ", d)
        print("pixel d:", pixeld)
                
    # FOR TESTING ONLY:
    img_name = "opencv_detected.png"
    cv.imwrite(img_name, frame)
    # print("done!")
    # center_x - middle of eyes (x), center_y - middle of eyes (y)
    offset = [310-target[0], 240-target[1]]
    print('Offset from goal position: ', offset)
    phys_offset = [offset[0]*pixeld, offset[1]*pixeld] #mm
    # determine the anlge the monitor must rotate
    pan = np.arctan(phys_offset[0]/d)
    tilt = np.arctan(phys_offset[1]/d)
    print(pan, tilt)
    return offset

def pixToDim(width_pix):
    focalLength = 3.85 #mm
    avg_width = 142.5 #mm
    F = 1080#(width_pix*600)/avg_width
    d = (avg_width*F)/width_pix #mm
    pixeld = avg_width/width_pix #mm
    #print("d=", F)
    return d, pixeld


parser = argparse.ArgumentParser(description='Code for Cascade Classifier tutorial.')
parser.add_argument('--face_cascade', help='Path to face cascade.', default='data/haarcascades/haarcascade_frontalface_alt.xml')
parser.add_argument('--eyes_cascade', help='Path to eyes cascade.', default='data/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
parser.add_argument('--camera', help='Camera divide number.', type=int, default=0)
args = parser.parse_args()
face_cascade_name = args.face_cascade
eyes_cascade_name = args.eyes_cascade
face_cascade = cv.CascadeClassifier()
eyes_cascade = cv.CascadeClassifier()

#-- 1. Load the cascades
if not face_cascade.load(cv.samples.findFile(face_cascade_name)):
    print('--(!)Error loading face cascade')
    exit(0)
if not eyes_cascade.load(cv.samples.findFile(eyes_cascade_name)):
    print('--(!)Error loading eyes cascade')
    exit(0)
    
#-- 2. Read the video stream
face = 0    # flag to indicate if face was detected
offset = (100,100)
cap = cv.VideoCapture(0, cv.CAP_DSHOW)
# OPTION 1
# Code for finding the face and calculating offset
# if not cap.isOpened:
#     print('--(!)Error opening video capture')
#     exit(0)
# while face == 0:
#     ret, frame = cap.read()
#     if frame is None:
#         print('--(!) No captured frame -- Break!')
#         break
#     face = detectFace(frame)
# cap.release()   # stop recording

# OPTION 2
### TESTING: continue finding face and offset until the face is within acceptable bounds fo the goal ###
while (offset[0] > 10 or offset[0] < -10) or (offset[1] > 10 or offset[1] < -10):
    if not cap.isOpened:
        print('--(!)Error opening video capture')
        exit(0)
    while face == 0:
        ret, frame = cap.read()
        if frame is None:
            print('--(!) No captured frame -- Break!')
            break
        face = detectFace(frame)
    cap.release()   # stop recording
########################################################################################################
    # unindent the code block below later
    #-- 3. Analyze the saved image
    path = r'opencv_frame.png'    # CHANGE FOR ODROID
    # Determine the difference between face and the target position
    offset = analyzeFrame(path)
    if (offset[0] < 10 or offset[0] > -10) or (offset[1] > 10 or offset[1] < -10):
        print('nope')
        face = 0
        cap = cv.VideoCapture(0, cv.CAP_DSHOW)
    # Convert digital offset to motor information
print('good', offset)

# Delete image when finished
os.remove(path)
```

We also found out from the machine shop that there are some limitations to the model they constructed. Despite the motors being rated to handle it, the tilting is limited by the weight of the load/monitor. Unfortunately, the stand can only support a tablet at most. This seems to be due to some parts the machine shop used. If a larger worm gear was used, it would be possible to have the stand meet our weight specifications. Also, the vertical adjustment does not meet our expectation because of the bracket that was used. Contacted the professor and TA, told that this would not be a problem.

<a name="2022-04-05 - Running on Odroid"></a>
# 2022-04-05 - Running on Odroid
To incorporate the code into our project, we will need to get the python code running on the ODROID XU4. 

First, the necessary packages had to be installed. This took some time to complete as there were issues with accessing Github and using pip. Ended up having to update an expired key. OpenCV was especially an issue. Since the ODROID used an older version of python (2.7), we had to ensure that all the packages were compatible. A few changes to the code were necessary. After installing OpenCV, Scipy, and imutils, we were able to run the code. One note was that the video capture argument had to be changed occasionally. When the Odroid was restarted or the USB camera was unplugged, the index would change. To fix this, the code must be manually updated each time it is run.

<a name="2022-04-06 - GPIO and WiringPi on Odroid"></a>
# 2022-04-06 - GPIO and WiringPi on Odroid
To communicate between the software and hardware portions of the project, we decided to use WiringPi. However, WiringPi is no longer supported by the original creator, so we needed to find a copied repository that was compatible with the ODROID and was still supported. WiringPi should allow us to easily use the GPIO pins on the Odroid and I2C.

Unable to install WiringPi and GPIO does not work properly. Will have to try again tomorrow.

<a name="2022-04-07 - GPIO and WiringPi on Odroid 2"></a>
# 2022-04-07 - GPIO and WiringPi on Odroid 2
Spent time trying to install GPIO and WiringPi on the Odroid again.

After some searching online, I found a GitHub repository with a WiringPi library for the Odroid that is maintained by Hardkernel: https://github.com/Pi4J/wiringPi-Odroid.

I was able to clone this repository and got GPIO and WiringPi to work on the Odroid XU4. I tested this by connecting a GPIO pin on the Odroid to a 1.8 V source and reading it with GPIO. I also tested it using wiringPi in a simple Python script. Everything functioned as expected.

One problem I noticed was that the library seemed to only have information for the Odroid XU4  with a shifter shield, which we do not have. This creates a different output when running GPIO readall. We will have to be careful when accessing different pins in case this becomes an issue later. Below are the pinouts showing the differences:
![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/odroid_pinout_comp.PNG)

Another problem I came across in this process is that without the shifter shield, the GPIO pins can only handle 1.8 V. Any higher of a voltage could damage it. We will have to make sure this is acceptable for the micrcontroller.

<a name="2022-04-08 - GPIO and WiringPi on Odroid 3"></a>
# 2022-04-08 - GPIO and WiringPi on Odroid 3
Found that the microcontroller requires a voltage closer to 3.3 V, which the Odroid is not capable of handling without a shifter shield. We looked online, but we could not find any shifter shields that we could buy that would arrive in time. The next option is to try to make our own shifter shield. Slight concern about the switching speed, but we'll try it.

<a name="2022-04-17 - Begin Switch to Raspberry Pi"></a>
# 022-04-17 - Begin Switch to Raspberry Pi
The homemade shifter shield did not work, so we are switching from the Odroid to a Rapsberry Pi 3B for the face detection code. Spent the day installing packages onto the Pi, downloading our code, and getting the code to run. We also decided to change the I2C communication from using WiringPi to SMBus.

<a name="2022-04-19 - Running on Raspberry Pi"></a>
# 2022-04-19 - Running on Raspberry Pi
Code is now running on the Pi. Spent some time testing it and making minor adjustments to the code. We will need to write the code for I2C communication at this point. Below is the pinout of our microcontroller:
![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/stm32_pinout1.png)
<br>Currently, the I2C slave address should be at 0x00 for the microcontroller. 

<a name="2022-04-20 - I2C Integration"></a>
# 2022-04-20 - I2C Integration
Went into lab to try to detect the address of the microcontroller using the Pi and i2cdetect. THe basic setup consists of the Raspberry Pi as the master and the STM32 as the slave. I2C has been enabled on the Pi and properly configurede. Additionally, the Pi has internal 1.8 kOhm resistors on the SDA and SCL lines, so no extra resistors are necessary. Strangely, the Pi seems unable to find it. We changed the primary slave address of the microcontroller to various addresses, double checked that the clock and data pins were properly connected, and tried grounding the Pi and microcontroller together. The Pi seemed to be functioning properly and was sending the data, but for some reason the microcontroller still could not be found. 

We tried probing the data line and the clock line and discovered that the clock is being held low by the microcontroller. 

Tried programming the microcontroller with a different sketch, and this time the clock was not held low immediately. After running i2cdetect, the clock went low again. It seems that reprogramming the microcontroller somehow randomly resets this? We are not sure why this is happening and will continue working on it tomorrow.

<a name="2022-04-21 - I2C Integration 2"></a>
# 2022-04-21 - I2C Integration 2
Spent more time in the lab trying to diagnose the problem with I2C. Still unable to find the microcontroller using the Pi. We are not able to write or read from the microcontroller using I2C. The clock still seems to be held low. Unfortunately, we do not have a logic analyzer that we can use to see what is being sent. After spending the day on this, we were unable to make further progress.

<a name="2022-04-22 - Mock Demo"></a>
# 2022-04-22 - Mock Demo
The mock demo went okay. The individual subsystems seem to be working. The face detection is able to find the user's face and determine the number of degrees the pan and tilt motors should move. 

The motor and processing subsystem functions as expected, and we are able to the pan/tilt motors a desired amount by setting the number of encoder clicks. 
* A positive encoder target value rotates the pan motor in the counterclockwise direction when viewed from the top, and a negative encoder target value rotates the pan motor in the clockwise direction when viewed from the top. 

* A positive encoder target value rotates the tilt motor so that the monitor tilts toward the ceiling, and a negative encoder target value rotates the tilt motor so that the monitor tilts down toward the ground.

The power subsystem works to power the microcontroller and the motors. We still have to integrate the AC/DC converter. As of now, we are still powering our project using the power supply in the lab.

After the Mock Demo, we spent more time trying to figure out why I2C was not working. Still nothing.

<a name="2022-04-23 - I2C Integration 3"></a>
# 2022-04-23 - I2C Integration 3
Returned to the lab to work on the problem with I2C. It still does not work, and we have no idea why. At this point, we will either have to switch the method of communication between the Pi and microcontroller, or we will have to demo the project as is. Integration at this point seems unlikely. 

<a name="2022-04-24 - Switch to SPI"></a>
# 2022-04-24 - Switch to SPI
After looking at our current PCB and the datasheet for the micrcontroller, it seems that SPI is a plausible alternative. Jake cut the traces from the I2C pins on our PCB and soldered wires on to try this method. Fortunately, we only need one-way communication from the Pi to the microcontroller, so we only need two wires for SPI to work: CLK and MOSI (master out slave in).
 
Jake tested SPI after soldering using a simple Python script and was able to communicate. We will be moving forward with SPI. 
<br>Finalized Pinout of the microcontroller:
![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/finalMCU_pinout.PNG)

We then calibrated the encoder to a degree value so that the data from the face detection on the Pi could be used. We did this by taking a picture of the initial motor position, running the pan motor by 10,000 encoder clicks, and taking a picture of the final motor position. 
<br>Initial and Final Motor Position:![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/motor_positions.PNG)
<br>Angle Between These Positions:![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/motor_change.jpg)
Based on these images, we were able to determine an angle of 23.03 degrees corresponds to 10,000. Therefore, after calculating the angle that the motors should pan and tilt in the Python code, we can multiply it 10,000/23.03 = 434.22 to determine how many encoder clicks the motors should move. This data will be sent using SPI.

<a name="2022-04-25 - Finishing Touches"></a>
# 2022-04-25 - Finishing Touches
Today, we will finishing testing that the project works overall, integrate the power subsytem, and clean up the overall project. 

The SPI was having some trouble sending consistent data, so we spent some time debugging this issue. The face detection was having some trouble giving consistent values because it would struggle to find the eyes on the face, so we changed the position it should use as an offset to only consider the center of the face. We also sped up the Python code by stopping it from repoening and closing the video stream so many times. The final version of the code is at the bottom of this entry.

The "remote" has been tested and works, and case has been found for the "remote" PCB. A slight modification of the micrcontroller code was made to fix an issue with the button presses. Any button press was being treated as an "adjust" command, so a specification was made in the if statement where the button press is checked so that no adjusting occured on the "up/down" button presses other than vertical motion.

The power subsystem has been set up so that the project is powered using a wall outlet. The AC/DC converter takes a 120 VAC input  and outputs two 12 V rails. One rail is used ot power the motors, and the other rail is connected to a buck converter that adjusts the output to 5 V. This 5 V rail is used to power other elements of the project including the microcontroller once it is stepped down.

The PCB has been placed in the wooden "housing", and longer, cleaner wires were cut to connect the PCB to the motors and tied with a ziptie. The power subsystem, the PCB, and the Raspberry Pi have all been placed in the box.

Ready for the demo.

<br>Final Project: ![]()

```
from __future__ import print_function
import cv2 as cv
import argparse
import numpy  as np
import time
import os
from scipy.spatial import distance as dist
from imutils import perspective
from imutils import contours
import imutils
import wiringpi as wpi
import smbus
import time
import math
import spidev
import time

panenc = 0
tiltenc = 0

def detectFace(frame):
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_gray = cv.equalizeHist(frame_gray)

    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    maxRect = 10000     # define the minimum area for a detected face
    face = 0
    # If a face is detected, save the image
    if len(faces) > 0:
        img_name = "opencv_frame.png"
        cv.imwrite(img_name, frame)
        face = 1

    return face

def analyzeFrame(imagePath):
    midpoint = [0,0] # midpoint between eyes
    eye_cnt = 0 # keep track of how many eyes
    frame = cv.imread(imagePath)
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_gray = cv.equalizeHist(frame_gray)

    #-- Detect faces
    faces = face_cascade.detectMultiScale(frame_gray)
    maxRect = 0     # define the minimum area for a detected face
    # for each face detected:
    for i in range(len(faces)):
        (x,y,w,h)=faces[i]
        area = np.pi*(faces[i][2]*faces[i][3])/4
        # work with the "biggest" face in the frame to avoid adjusting to a background person
        if area > maxRect:
            maxRect = area
            center = (x + w//2, y + h//2)
            # Draw a circle around the face (TESTING)
            frame = cv.ellipse(frame, center, (w//2, h//2), 0, 0, 360, (0, 255, 0), 4)
            faceROI = frame_gray[y:y+h,x:x+w]
            #-- Detect eyes for the closest/largest face
            eyes = eyes_cascade.detectMultiScale(faceROI)
            for j in range(len(eyes)):
                (x2,y2,w2,h2) = eyes[j]
                eye_cnt += 1
                eye_center = (x + x2 + w2//2, y + y2 + h2//2)
                midpoint[0] += eye_center[0]
                midpoint[1] += eye_center[1]
                # Draw a circle around the eyes (TESTING)
                radius = int(round((w2 + h2)*0.25))
                frame = cv.circle(frame, eye_center, radius, (255, 0, 0 ), 4)
            # Determine the point on the face that should be centered with the monitor
            #if(eye_cnt == 2):
             #   target = (midpoint[0]//2, midpoint[1]//2)
            #elif(eye_cnt == 1):
            #    target = (x + w//2, midpoint[1])
            #else:
            target = (x + w//2, y + h//2 - h//4)
    # Determine conversion of pixels to physical space
        d, pixeld = pixToDim(w)
        # TESTING ONLY
        #print("d: ", d)
        #print("pixel d:", pixeld)    

# FOR TESTING ONLY:
    img_name = "opencv_detected.png"
    cv.imwrite(img_name, frame)
    # print("done!")
    # Find the pixel offset between the center of the screen and the target on the person's face
    # NEED TO ADD AN OFFSET FOR THE CAMERA POSITION AFTER INTEGRATION
    # offset = [center_x - middle of eyes (x), center_y - middle of eyes (y)]
    offset = [310-target[0], 240-target[1]]
    #print('Offset from goal position: ', offset) # FOR TESTING
    phys_offset = [offset[0]*pixeld, offset[1]*pixeld] #mm
    # determine the anlge the monitor must rotate
    pan = np.arctan(phys_offset[0]/d) #radians
    tilt = np.arctan(phys_offset[1]/d) #radians
    # print(pan, tilt) # FOR TESTING
    return pan, tilt

# Using perceived value of F, calculate the distance the user is from the camera and mm/pixel using the similar triangles method
def pixToDim(width_pix):
    focalLength = 3.85 #mm
    avg_width = 142.5 #mm
    F = 1080#(width_pix*600)/avg_width
    d = (avg_width*F)/width_pix #mm
    pixeld = avg_width/width_pix #mm
    #print("d=", F)
    return d, pixeld

# function for sending data by SPI

def write_targets(t1, t2):
    msb1 = (t1 >> 8) & 0xFF
    lsb1 = t1 & 0xFF

    msb2 = (t2 >> 8) & 0xFF
    lsb2 = t2 & 0xFF
    spi.writebytes([msb1, lsb1, msb2, lsb2])


parser = argparse.ArgumentParser(description='Code for Cascade Classifier tutorial.')
parser.add_argument('--face_cascade', help='Path to face cascade.', default='data/haarcascades/haarcascade_frontalface_alt.xml')
parser.add_argument('--eyes_cascade', help='Path to eyes cascade.', default='data/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
parser.add_argument('--camera', help='Camera divide number.', type=int, default=0)
args = parser.parse_args()
face_cascade_name = args.face_cascade
eyes_cascade_name = args.eyes_cascade
face_cascade = cv.CascadeClassifier()
eyes_cascade = cv.CascadeClassifier()



#-- 1. Load the cascades`
if not face_cascade.load(face_cascade_name):
    print('--(!)Error loading face cascade')
    exit(0)
if not eyes_cascade.load(eyes_cascade_name):
    print('--(!)Error loading eyes cascade')
    exit(0)
    
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = 50000
spi.mode = 0
    
#-- 2. Read the video stream
cap = cv.VideoCapture(-1)
while(1):
    #print("check")
    face = 0    # flag to indicate if face was detected
    offset = (100,100)
    
    #if(cap.isOpened()==False):
    #    print("error")
    #while (cap.isOpened()):
    #    ret, frame = cap.read()
    #    if ret == True: 
    #        cv.imshow('Frame', frame)
    #        if cv.waitKey(25)&oxFF==ord('q'):
    #            break
    #        else:
    #            break

    # Code for finding the face and calculating offset
    print("ok")
    if not cap.isOpened:
            print('--(!)Error opening video capture')
            exit(0)
    while face == 0:
            ret, frame = cap.read()
            if frame is None:
                    print('--(!) No captured frame -- Break!')
                    break
            face = detectFace(frame)
       # stop recording

            # unindent the code block below later
    #-- 3. Analyze the saved image
    path = r'opencv_frame.png'    # CHANGE FOR ODROID
    # Determine the difference between face and the target position
    pan, tilt = analyzeFrame(path)
    panenc = int(math.degrees(pan)*434.22)
    tiltenc = int(math.degrees(tilt)*434.22)
        
    print('Pan: ', panenc, 'tilt: ', tiltenc)
    #print('the pan in degrees should be',panint)
    #print('the tilt in degrees is', tiltdeg)

    #pan_bytes = pandeg.to_bytes(1, 'big', signed=True)
    #tilt_bytes = tiltdeg.to_bytes(1, 'big', signed=True)

    #print(pan_bytes)
    #print(tilt_bytes)

    # Send data to the MCU
    #bus = smbus.SMBus(1) #creates a new smbus
    #address = 0x0 #address pf tje microcontroller
    #def write(value):
    #        value = value & 0xff
    #        bus.write_byte_data(address, 0, value)
    #        print("wrote value into bus??")
    #        return -1

#bus.write_i2c_block_data(address, 0x01, [panint, tiltdeg])

    write_targets(panenc, tiltenc)
    
    print('written')
    # Delete image when finished
    os.remove(path)
    #print("done")
```
<a name="2022-04-26 - Demo"></a>
# 2022-04-26 - Demo
The demo went pretty well. There were a couple of hiccups with the face detection finding something that wasn't a face, but we were able to demonstrate the expected functionality and explain most of the issues/glitches.

Final version of the block diagram for the presentation/report:
![](https://gitlab.engr.illinois.edu/annam4/self-adjusting-monitor-stand/-/raw/main/notebooks/anna/blockDiagram_finalfinal.png)
