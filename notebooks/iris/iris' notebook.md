* [January 17th 2022](#1/17/2022)
* [January 28th 2022](#1/28/2022)
* [Feburary 4th 2022](#2/04/2022)
* [Feburary 10th 2022](#2/10/2022)
* [Feburary 26th 2022](#2/26/2022)
* [March 1st 2022](#3/01/2022)
* [March 12th 2022](#3/12/2022)
* [April 14th 2022](#4/14/2022)
* [April 17th 2022](#4/17/2022)
* [April 22nd 2022](#4/22/2022)
* [May 2nd 2022](#5/02/2022)

<a name="1/17/2022"></a>
# 1/17/2022 
<br>Thinking of project ideas</br>
* <b>automatic color-matcher/paint mixer</b>\
 https://blog.prototypr.io/q-a-how-can-digital-designers-mix-rgb-colors-more-effectively-74aa6915ed97 \
 -might be difficult to mix because you would need either a large amount of paint (could dry up when doing testing) or a very narrow container to mix them in\
 -probably needs more from the machine shop - go talk to them

* <b>running alarm clock</b>\
 -detects where someone is in the room (maybe paired w/ watch?)\
 -might just be glorified ece110 project though.
 
* <b>blinds that change with sun direction</b>
 -slowly raises the blinds depending on where the sun is \
 -motor attached to the tilt want and the one that raises/lowers the blinds (or pushing version?) \
 -might have issues demoing? 

* <b>automatic catcher/thrower</b>\
-plays catch with the user\
-some kind of net that an arm can go through\

<a name="1/28/2022"></a>
# 1/28/2022 
<br>Talked to Gregg at the machine shop</br>
To keep in mind while choosing components to hand to machine shop
* motors need enough torque (MONITOR WEIGHT: ~4kg)
* worm gears (machine shop will handle; at least 4:1 reduction?)
* My sketch of what our design might look like:
* ![image.png](notebooks/iris/z_monsketch.png)

<a name="2/04/2022"></a>
# 2/04/2022
<br>Added main block diagram components</br>
Image: Block diagram
![image.png](notebooks/iris/z_blockdiagram.png)

<a name="2/10/2022"></a>
# 2/10/2022
<br>Learning about h-bridges and bootstrap circuit
* https://www.modularcircuits.com/blog/articles/h-bridge-secrets/h-bridges-the-basics/ \
Image: Forward and Reverse driving of the motors\
![image](notebooks/iris/z_forwarddrive.png) \
![image](notebooks/iris/z_charging.png)

<a name="2/26/2022"></a>
# 2/26/2022
<br>Choosing Components for PCB 
choosing capacitors/MOSfets/resistors for half drivers (subject to change if we find the diodes or MOSfets are too large for the board, or out of stock) \
T = Rboot * Cboot / Duty Cycle\
Cg = 20nC/(12V-.48V) \
Cboot > 10*Cg , choose <b>Cboot to be 18nF</b> \
As for Rboot, T = Rboot * Cboot / Duty Cycle \
Where Duty Cycle = .9, Cboot = 18nF, and T is 5e-6 seconds because off-time should be under 10% of 1/frequency  \
Rboot < 250 ohms, so we choose <b>Rboot = 100 ohms</b> due to availability in lab \
Checking Ipeak fits requirements; Ipeak = (Vdd-Vbootdiode)/Rboot = .115A

<a name="3/01/2022"></a>
# 3/01/2022
<br>Design Review Revisions to be done
* make the colors more readable on block diagram
* clarity on remote module (wired)
* how often is the webcam to be triggered? consider invasiveness
* justify the 8 seconds needed for the monitor to adjust
* tolerance analysis should be more about math/simulations backing the idea
Final edits on block diagram to include a few more signals
![image.png](notebooks/iris/z_finalbd.png)

<a name="3/12/2022"></a>
# 3/12/2022
Researching on OpenCV and what camera to use\
https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html\
Haar cascades using Haar features seems to be an efficient way of finding faces as it's more about the edges of features rather than finding many points on a face and recognizing that way. This differentiates the background and the subject quicker and will be sufficient for the project.\
![image](notebooks/iris/z_haar_feat.png)
Finds the difference of the shaded vs unshaded areas. Usually can make it so only 200 of these features are needed and still deliver 95% accuracy.

<a name="4/14/2022"></a>
# 4/14/2022
Problem: Communicating with STM32 (I2C)\
can also use SPI as last resort.\
Use wiringPi to setup GPIO on XU4: http://wiringpi.com/ developer has stopped supporting wiringPi so just cloned from a github \
Python library smbus (I2C): https://www.abelectronics.co.uk/kb/article/1/i2c-part-2---enabling-i-c-on-the-raspberry-pi \
Despite having the I2C Pins connected to the correct pins on the microcontroller and Pi, this function still outputs the below table, indicating these pins are unable to be found.

    sudo i2cdetect -y 1
         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- --
    10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    70: -- -- -- -- -- -- -- --

Tutorial for communicating with python library smbus: https://www.abelectronics.co.uk/kb/article/1094/i2c-part-4---programming-i-c-with-python 

        i2cbus.write_byte_data(i2caddress, GPIOA, 0x01)  # Set pin 1 to on
        time.sleep(0.5)  # Wait 500ms

        i2cbus.write_byte_data(i2caddress, GPIOA, 0x00)  # Set pin 1 to off
        time.sleep(0.5)  # Wait 500ms
<a name="4/17/2022"></a>
# 4/17/2022
Problem: Odroid XU4 does not output 3.3V needed to be read by STM32\
Solutions: level shifter or amplifier?\
Amplifier will not work because won't be able to tell what direction the motors should rotate in \
Need level shifter for the Odroid XU4 to output 3.3V, not 1.8V\
Image: Bi-Directional Level shifter \
![image](notebooks/iris/z_bidirlevelshifter.png)
Unfortunately we do not have the parts readily available, and this would require using a breadboard which would add to the bulkiness of our project. \

Decided to change to Raspberry Pi that we had available instead.
<a name="4/22/2022"></a>
# 4/22/2022
Recording dimensions for fabrication lab cuts \
Dimensions of remote PCB part: 4cm x 2.5cm (+1/8 in. on each side from bandsaw) \
Acrylic stand-in monitor: 8in * 6in

Image: PCB Picture\
![image.png](notebooks/iris/z_remote.png) \
Image: Acrylic stand\
![image.png](notebooks/iris/z_acrylic.png)

<a name="5/02/2022"></a>
# 5/02/2022
Started editing the extra credit video. Report/Slides are also in progress.
