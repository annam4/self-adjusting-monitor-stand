# Activities Performed on April 23, 2022

## Switching to Serial Peripheral Interface (SPI)

After a lot of debugging with the STM32's onboard I2C peripheral, it was decided that with the demo approaching,
our resources would be better spent switching to a different communication protocol. However, this was slightly
problematic, as the PCB only had connections to the I2C pins, and not to another protocol, SPI. This problem
could be solved by soldering jumper wires to the SPI pins, but this procedure was somewhat risky. To ensure that 
this would work before ruining our one good board, I decided to solder another of our 3 STM32G0 microcontrollers
to another PCB. All I had to solder on this board was the microcontroller, a few terminal blocks to hook wires to,
and the LDO linear regulator to power the chip. Once this was complete, I created a simple dummy sketch meant to test 
SPI communication. 

![SPI jumper wires](Images/spi_jumper.jpg)

After a bit of fiddling with SPI settings on the Pi and STM, I was able to receive data successfully! Traditionally,
SPI is a unidirectional duplex communication protocol, which means that each data direction requires a separate wire.
This differs from I2C, which only has one wire for bidirectional communication. However, for our purposes, only one 
communication direction was needed anyway (Pi to microcontroller), so this line could be hooked up to the old I2C data line.
Once it was confirmed that this new protocol worked, I proceeded to apply this fix to the main PCB. Upon testing in the main
system, data was received by the STM32! However, this data would sometimes come in garbled, or shifted by a seemingly
random number of bits. It was eventually determined that this was due to signal integrity, which was solved by connecting
the Pi's ground with the PCB's. Once this fix was completed, we could successfully send 4 bytes of data in bursts,
which corresponded to motor targets. (16 bits for each target) I then added some code into the rising edge interrupt 
routine to read this SPI data.

```c
void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == ENC1_A_Pin) {
    	if (GPIOA->IDR & ENC1_B_Pin) {
    	    enc1_reading -= 1;
    	} else {
    	    enc1_reading += 1;
    	}
    } else if (GPIO_Pin == ENC2_A_Pin) {
    	if (GPIOA->IDR & ENC2_B_Pin) {
    	    enc2_reading -= 1;
    	} else {
    	    enc2_reading += 1;
    	}
    } else if (GPIO_Pin == MON_ADJ_Pin) {
    	// First read data from I2C
    	HAL_SPI_Receive(&hspi1, spi_data, sizeof(spi_data), 100);
    	// Now set target positions (deltas)
    	enc1_target = ((int)(spi_data[0]) << 8) + spi_data[1];
    	enc2_target = ((int)(spi_data[2]) << 8) + spi_data[3];
    }
}
```

This new interrupt source was a button on our wired remote, which served as the "adjust" button. Once this button was pressed,
the system would start receiving data from the Pi, which would be saved in the encoder target registers. However, 
it seemed that this button as an interrupt source was slightly unreliable. It was hard to tell when the button press was 
received properly by the hardware. To solve this issue, I added a directive to "wiggle" the pan motor very slightly once
the button press was received, then start receiving SPI data. This gave us a visual and audible metric to know if the 
button needed to be re-pressed. Once this was applied, we could reliably transfer data between the Pi and the STM32. 
