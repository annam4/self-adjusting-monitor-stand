# Activities Performed on April 25, 2022

Today is the day before our demo, so it was spent making the final preparations to our system.
Longer wires were cut, so as to be able to place the PCB and Pi under the monitor stand in the 
cavity provided. Wires were soldered onto the 12V to 5V buck converter, so that we could 
attach it to the PCB and Power subsystem. Finally, we set up the Pi to detect faces and 
tested the whole system. Everything seemed to go as planned, though we required a smooth 
background to ensure that no false face detections were made against the background. 
I believe that we are ready for the final demo tomorrow!

## Microcontroller Code

I am including the full STM32 main C file here for documentation purposes.

```c
/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */

volatile long enc1_reading;
volatile long enc2_reading;
volatile long enc1_target;
volatile long enc2_target;
volatile int command1;
volatile int command2;
volatile uint8_t spi_data[4];

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM1_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */
void MotorControl(unsigned int m1_speed, int m1_dir, unsigned int m2_speed, int m2_dir,
		unsigned int m3_speed, int m3_dir);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// For direction, 0 is forward, 1 is reverse
// Speed is divided by 5, maximum is 17 (85% duty cycle)
void MotorControl(unsigned int m1_speed, int m1_dir, unsigned int m2_speed, int m2_dir,
		unsigned int m3_speed, int m3_dir) {

	int m1_speed2, m2_speed2, m3_speed2;

	if (m1_speed > 17) m1_speed2 = 17;
	else m1_speed2 = m1_speed;
	if (m2_speed > 17) m2_speed2 = 17;
	else m2_speed2 = m2_speed;
	if (m3_speed > 17) m3_speed2 = 17;
	else m3_speed2 = m3_speed;

	volatile uint16_t timer_val = __HAL_TIM_GET_COUNTER(&htim1);

	// at start of PWM cycle, all bridges should be off
	if (timer_val == 0) {
		GPIOB->ODR = 0;
	}
	// one tick later, low sides should turn on
	if (timer_val == 1) {
		GPIOB->ODR = PWM1_C_Pin|PWM1_D_Pin|PWM2_C_Pin|PWM2_D_Pin|PWM3_C_Pin|PWM3_D_Pin;
	}

	if (m1_speed2 > 0) {
		if (m1_dir == 0) {
			if (timer_val >= (20 - m1_speed2 - 1)) {
				GPIOB->ODR &= ~PWM1_C_Pin; // turn off 1 low side
			}
			if (timer_val >= (20 - m1_speed2)) {
				GPIOB->ODR |= PWM1_A_Pin; // turn on 1 high side
			}
		} else {
			if (timer_val >= (20 - m1_speed2 - 1)) {
				GPIOB->ODR &= ~PWM1_D_Pin; // turn off 1 low side
			}
			if (timer_val >= (20 - m1_speed2)) {
				GPIOB->ODR |= PWM1_B_Pin; // turn on 1 high side
			}
		}
	}

	if (m2_speed2 > 0) {
		if (m2_dir == 0) {
			if (timer_val >= (20 - m2_speed2 - 1)) {
				GPIOB->ODR &= ~PWM2_C_Pin; // turn off 2 low side
			}
			if (timer_val >= (20 - m2_speed2)) {
				GPIOB->ODR |= PWM2_A_Pin; // turn on 2 high side
			}
		} else {
			if (timer_val >= (20 - m2_speed2 - 1)) {
				GPIOB->ODR &= ~PWM2_D_Pin; // turn off 2 low side
			}
			if (timer_val >= (20 - m2_speed2)) {
				GPIOB->ODR |= PWM2_B_Pin; // turn on 2 high side
			}
		}
	}

	if (m3_speed2 > 0) {
		if (m3_dir == 0) {
			if (timer_val >= (20 - m3_speed2 - 1)) {
				GPIOB->ODR &= ~PWM3_C_Pin; // turn off 3 low side
			}
			if (timer_val >= (20 - m3_speed2)) {
				GPIOB->ODR |= PWM3_A_Pin; // turn on 3 high side
			}
		} else {
			if (timer_val >= (20 - m3_speed2 - 1)) {
				GPIOB->ODR &= ~PWM3_D_Pin; // turn off 3 low side
			}
			if (timer_val >= (20 - m3_speed2)) {
				GPIOB->ODR |= PWM3_B_Pin; // turn on 3 high side
			}
		}
	}

}

void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == ENC1_A_Pin) {
    	if (GPIOA->IDR & ENC1_B_Pin) {
    	    enc1_reading -= 1;
    	} else {
    	    enc1_reading += 1;
    	}
    } else if (GPIO_Pin == ENC2_A_Pin) {
    	if (GPIOA->IDR & ENC2_B_Pin) {
    	    enc2_reading -= 1;
    	} else {
    	    enc2_reading += 1;
    	}
    } else if (GPIO_Pin == MON_ADJ_Pin) {
    	// First read data from I2C
    	HAL_SPI_Receive(&hspi1, spi_data, sizeof(spi_data), 100);
    	// Now set target positions (deltas)
    	enc1_target = ((int)(spi_data[0]) << 8) + spi_data[1];
    	enc2_target = ((int)(spi_data[2]) << 8) + spi_data[3];
    }
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_TIM1_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
  __TIM1_CLK_ENABLE();
  HAL_TIM_Base_Start(&htim1);
  enc1_reading = 0;
  enc2_reading = 0;

  enc1_target = 7;
  enc2_target = 0;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  // At startup, encoders are zeroed out

	  command1 = enc1_target - enc1_reading;
	  command2 = enc2_target - enc2_reading;

	  // If we've reached the target, then zero the targets and current positions

	  if (command1 < 5 && command2 < 5) {
		  enc1_reading = 0;
		  enc2_reading = 0;
		  enc1_target = 0;
		  enc2_target = 0;
	  }


	  // Check to see if we're moving up or down

	  int command3 = (GPIOD->IDR & (MON_UP_Pin|MON_DOWN_Pin)) > 0;

	  MotorControl(abs(command1), command1<0, abs(command2), command2<0,
	  			  command3*17, GPIOD->IDR & MON_UP_Pin);


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 16;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  /** Enables the Clock Security System
  */
  HAL_RCC_EnableCSS();
}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  hrtc.Init.OutPutPullUp = RTC_OUTPUT_PULLUP_NONE;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_SLAVE;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES_RXONLY;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 160 - 1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 20 - 1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, PWM1_A_Pin|PWM1_B_Pin|PWM1_C_Pin|PWM3_C_Pin
                          |PWM3_D_Pin|PWM1_D_Pin|PWM2_A_Pin|PWM2_B_Pin
                          |PWM2_C_Pin|PWM2_D_Pin|PWM3_A_Pin|PWM3_B_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : ENC1_A_Pin ENC2_A_Pin */
  GPIO_InitStruct.Pin = ENC1_A_Pin|ENC2_A_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : ENC1_B_Pin ENC2_B_Pin */
  GPIO_InitStruct.Pin = ENC1_B_Pin|ENC2_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PWM1_A_Pin PWM1_B_Pin PWM1_C_Pin PWM3_C_Pin
                           PWM3_D_Pin PWM1_D_Pin PWM2_A_Pin PWM2_B_Pin
                           PWM2_C_Pin PWM2_D_Pin PWM3_A_Pin PWM3_B_Pin */
  GPIO_InitStruct.Pin = PWM1_A_Pin|PWM1_B_Pin|PWM1_C_Pin|PWM3_C_Pin
                          |PWM3_D_Pin|PWM1_D_Pin|PWM2_A_Pin|PWM2_B_Pin
                          |PWM2_C_Pin|PWM2_D_Pin|PWM3_A_Pin|PWM3_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : MON_ADJ_Pin */
  GPIO_InitStruct.Pin = MON_ADJ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(MON_ADJ_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : MON_UP_Pin MON_DOWN_Pin */
  GPIO_InitStruct.Pin = MON_UP_Pin|MON_DOWN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);

  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
```
