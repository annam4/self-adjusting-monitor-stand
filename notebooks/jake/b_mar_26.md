# Activities Performed on March 26, 2022

## Finished Soldering PCB

Today I finished assembling our group's PCB. There were a few components we were unable to source, so I had to make do with what I could find in the 445 lab.
For example, C1 in our schematic is supposed to be a 2.2 uF capacitor, but this value was not present. To make do, I sourced 2 1 uF capacitors from our stock, and soldered them in parallel on the original pads to form a 2 uF capacitor. This value is more than sufficient, as this capacitor only filters high frequency noise from the 5 V line. 

## First PCB Tests and Issues Fixed

![microcontroller schematic](Images/button.png)

On our first attempt at powering up the microcontroller, we were unsuccessful. C31, as seen in the above schematic, was intended to provide filtering between the analog and digital ground planes. However, I should have connected this component differently, because in the intended configuration, it decouples these ground planes from each other, preventing DC ground current from flowing back to the power supply. Removing this capacitor and replacing with a 0 ohm resistor solved the first powerup issue.

The next issue came in the form of the reset button being incorrectly configured. I assumed that our button would short the left and right sides when pressed, but instead it shorted the top and bottom sets of pins. This led to the reset line being permanently tied to ground through the button, leaving the microcontroller in a nonfunctional state. Removing the reset button solved this issue, and we were able to communicate with it using the ST-LINK V2 programmer.

 
