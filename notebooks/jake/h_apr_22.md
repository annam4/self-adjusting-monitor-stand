# Activities Performed on April 22, 2022

Today was spent attempting to get I2C communication working between our microcontroller and the Odroid XU4. 
First, it was determined that the Odroid uses a logic level of 1.8 V, while our microcontroller uses 3.3 V logic. 
These are obviously incompatible, but I believed that we could use components found in the lab to construct
a simple level shifter. Such a design consists of a single MOSFET for each line to shift, and operates 
in a bidirectional manner. However, the lab did not have MOSFETs which fit our specifications for response speed
and input voltage. This meant that we had to switch from the Odroid XU4 to a Raspberry Pi 3B, which uses 3.3 V logic.
This was not a significant hassle, as our computer vision code runs on Python, which is extremely platform agnostic. 

After switching from the Odroid to the Pi, the next item of business was to connect over I2C.
In the STM32CubeMX configurator, I initialized the onboard I2C peripheral on pins PA5 and PA10. 
After connecting the Pi's I2C pins to the terminal block on our board for I2C, I attempted to use
the 'i2cdetect' command bundled with the Pi to search for the connected device. This utility 
sends out I2C packets to every possible address, and listens for a reply. Once a reply is received,
it marks that address as 'up', and lists it as such on the screen. However, we were unable to 
get the Pi to recognize the connected STM32. I did notice however that when the STM was unplugged,
this utility ran very quickly, while when it was connected, it ran much more slowly. Clearly,
there was some sort of interaction going on, I just wasn't quite sure what. 

By using a digital logic analyzer, I was able to see that the STM32 appeared to be constantly pulling the 
SCLK line low whenever powered on. This behavior is consistent with a feature known as 'clock stretching', 
which is when a slave device is not ready to receive data from the master. It communicates the need to wait
by bringing the clock line low until it is ready. However, even after disabling clock stretching on the STM, 
this behavior persisted. Eventually, I discovered that when the ST-LINK V2 programmer was connected, the clock
line would stay low. However, when the programmer was disconnected, the clock line would return to normal. 
This led me to believe that there was some sort of conflict between the SWD programming interface used to 
program the STM32, and the I2C peripheral. After many hours of debugging, we were overall unsuccessful in
getting Pi-\>STM32 communication working. 
