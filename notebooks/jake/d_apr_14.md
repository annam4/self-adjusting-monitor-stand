# Activities Performed on April 14, 2022

## Live Motor Driver Test 

Last week the extent of our motor driver testing was limited to viewing the unloaded output, at a medium duty cycle. 
This time, it was necessary to investigate the efficacy of the drivers when connected to a true load.
There were several issues under investigation here, including the resistive and inductive effects of the motors on the drivers.
When driving DC motors with MOSFET H-bridges, there must be some dead time between forward operation and braking, which happens every PWM cycle.
During this dead time, an unclamped motor acts as an inductor, and the voltage across the motor becomes extremely negative.
This inductive kick must be clamped to safe levels using external "flyback" diodes in reverse across each MOSFET, otherwise component
damage may result. This system could possibly not have worked, and so this test was ensuring that the drivers would not be damaged under load.
The motors were run at maximum duty cycle (85%) with no issues. The below figure shows the PWM signals sent during this time. 

![full power](Images/livemotor.jpg)

## Encoder Investigation

During today's activities, I also attempted to read the value of our rotary encoders using the STM32. 
The basic operation of quadrature encoders is as follows:
For each encoder tick, one of the two data lines sends a rising edge. The other line does the same, but shifted 90 degrees out of phase.
The direction of the phase shift shows the direction of rotation. 

```c
void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == ENC1_A_Pin) {
    	if (GPIOA->IDR & ENC1_B_Pin) {
    	    enc1_reading -= 1;
    	} else {
    	    enc1_reading += 1;
    	}
    } else if (GPIO_Pin == ENC2_A_Pin) {
    	if (GPIOA->IDR & ENC2_B_Pin) {
    	    enc2_reading -= 1;
    	} else {
    	    enc2_reading += 1;
    	}
    } 
}
```

The above code is the rising edge interrupt callback I implemented to track encoder positions. In the STM32CubeMX configuration tool, I have set up the ENC1\_A and ENC2\_A pins 
as rising edge interrupt sources. When either of these pins sees a rising edge, this function will be called. Depending on the source of the interrupt, either the 
value of encoder 1 or encoder 2 will be modified. Below is an explanation of how the direction is determined.

![encoder signals](Images/encoder.jpg)

To read this data on the STM32, we can set up the "A" channel of each encoder to trigger a rising edge interrupt. Then, we can read the value of the "B"
channel at this time, and use that to determine direction. If direction is positive, add 1 to a global encoder tracking variable, and subtract 1 if 
direction is negative. I ran the motor in question in the "positive" direction, and used that to determine which state of the "B" pin corresponded
to a positive direction. By watching the encoder position variable in the STM32CubeIDE live expression viewer, I was able to verify that this code
was working properly, and counting every encoder tick.
