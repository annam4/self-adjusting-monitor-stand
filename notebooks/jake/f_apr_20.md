# Activities Performed on April 20, 2022

Today I attempted to test our microcontroller motor control code. My original idea for a simple control loop was to use only proportional control. 
In this way, the command given to the motors is only dependent on the difference between the target position and the current position. 
This method requires the least amount of computation, so seemed the best to use. However, our motor command saturates at a value of 17,
because the command represents 5% steps in duty cycle, stopping at a max of 85%. This means that since the encoder values are often quite high,
we would nearly always be saturating the motor command. To counteract this, I thought I would use a fractional "P" constant multiplied by
the target and current position difference, as outlined in the following code block. 

```c
command1 = enc1_target - enc1_reading;
command2 = enc2_target - enc2_reading;

P = 0.01;

MotorControl(abs(command1*P), command1<0, abs(command2*P), command2<0, 0, 0);
```

This method was unsuccessful. Once the system was turned on, I could hear the motors whirring, but not moving. After scoping the output and control signals, 
I determined that the correct PWM signals were not being sent to the motor drivers. This turned out to be the result of a slowdown in the microcontroller.
The STM32G0 chip we selected for this project does not have a Floating Point Unit or FPU, which means it must rely on GCC's software floating point implementation.
This significantly increases the computation time, and thus ruins the PWM signal generation. To fix this, I set the proportional constant to 1, so no floating
point math was required. This sped up the computation enough, and resulted in excellent control loop performance. No overshoot was observed, and response
speed was excellent. 
