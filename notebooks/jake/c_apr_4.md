# Activities Performed on April 4, 2022

## PCB Debugging

In the last update, I detailed how we were able to interface with the microcontroller using the debugger. However, the next challenge was to actually execute code on it. This proved to be much more difficult than expected. At first, code seemed to upload correctly to the device, but did not run. Debugging was also not working. This issue turned out to be a result of a nonfunctional 8 MHz crystal resonator configuration. The loading capacitors placed on this resonator were incorrect values, and thus the crystal did not oscillate properly. Replacing the 4700 pF capacitors with the proper 10 pF components solved this issue, and got the clock resonating.

![clock running on scope](Images/clock.jpg)

It can be seen from this scope trace that the oscillator is running at very nearly exactly 8 MHz, which bodes well for our project.

After this main issue was resolved, it was time to start developing the motor control code to drive the H-bridges. The output from my first draft of this code is pictured below. 

![no dead time](Images/scope1.jpg)

```c
void MotorControl(unsigned int m1_speed, int m1_dir, unsigned int m2_speed, int m2_dir,
		unsigned int m3_speed, int m3_dir) {

	int m1_speed2, m2_speed2, m3_speed2;

	if (m1_speed > 17) m1_speed2 = 17;
	else m1_speed2 = m1_speed;
	if (m2_speed > 17) m2_speed2 = 17;
	else m2_speed2 = m2_speed;
	if (m3_speed > 17) m3_speed2 = 17;
	else m3_speed2 = m3_speed;

	volatile uint16_t timer_val = __HAL_TIM_GET_COUNTER(&htim1);

	// at start of PWM cycle, all bridges should be off
	if (timer_val == 0) {
		GPIOB->ODR = 0;
	}
	// one tick later, low sides should turn on
	if (timer_val == 1) {
		GPIOB->ODR = PWM1_C_Pin|PWM1_D_Pin|PWM2_C_Pin|PWM2_D_Pin|PWM3_C_Pin|PWM3_D_Pin;
	}

	if (m1_speed2 > 0) {
		if (m1_dir == 0) {
			if (timer_val >= (20 - m1_speed2 - 1)) {
				GPIOB->ODR &= ~PWM1_C_Pin; // turn off 1 low side
			}
			if (timer_val >= (20 - m1_speed2)) {
				GPIOB->ODR |= PWM1_A_Pin; // turn on 1 high side
			}
		} else {
			if (timer_val >= (20 - m1_speed2 - 1)) {
				GPIOB->ODR &= ~PWM1_D_Pin; // turn off 1 low side
			}
			if (timer_val >= (20 - m1_speed2)) {
				GPIOB->ODR |= PWM1_B_Pin; // turn on 1 high side
			}
		}
	}

	if (m2_speed2 > 0) {
		if (m2_dir == 0) {
			if (timer_val >= (20 - m2_speed2 - 1)) {
				GPIOB->ODR &= ~PWM2_C_Pin; // turn off 2 low side
			}
			if (timer_val >= (20 - m2_speed2)) {
				GPIOB->ODR |= PWM2_A_Pin; // turn on 2 high side
			}
		} else {
			if (timer_val >= (20 - m2_speed2 - 1)) {
				GPIOB->ODR &= ~PWM2_D_Pin; // turn off 2 low side
			}
			if (timer_val >= (20 - m2_speed2)) {
				GPIOB->ODR |= PWM2_B_Pin; // turn on 2 high side
			}
		}
	}

	if (m3_speed2 > 0) {
		if (m3_dir == 0) {
			if (timer_val >= (20 - m3_speed2 - 1)) {
				GPIOB->ODR &= ~PWM3_C_Pin; // turn off 3 low side
			}
			if (timer_val >= (20 - m3_speed2)) {
				GPIOB->ODR |= PWM3_A_Pin; // turn on 3 high side
			}
		} else {
			if (timer_val >= (20 - m3_speed2 - 1)) {
				GPIOB->ODR &= ~PWM3_D_Pin; // turn off 3 low side
			}
			if (timer_val >= (20 - m3_speed2)) {
				GPIOB->ODR |= PWM3_B_Pin; // turn on 3 high side
			}
		}
	}

}
```

This code operates off of one of the STM32's internal hardware timers. I have configured this timer to run at a frequency of 400 kHz. This is because we want our eventual PWM frequency to be 20Khz, 
and we would like to adjust the PWM duty cycle in increments of 5%. Thus, we must be able to time events at a frequency of 1/5% = 20 times faster than 20 kHz, or 400 kHz. 
This motor control function takes the desired speed and direction for each motor, and waits until the value of the hardware timer is at the proper value to change the driver controls. 
This allows me to precisely set a dead time of 2.5 us between the activation of the high and low sides of each half bridge, which is more than enough to account for the switching speed
of each MOSFET. 

Due to a coding error, there is no dead time on the falling edge of the PWM cycle. This is critical, as dead time allows for switching time to ensure the high and low sides of the half bridge are never on simultaneously. This avoids damaging shoot-through current, where the full supply voltage is shorted across the MOSFETs. This issue was rectified, and the resulting output is pictured below.

![yes dead time](Images/scope2.jpg)

Once this basic operation was verified, it was time to apply real 12 V to the motor drivers, and investigate the results. This was a complete success, and a nice 25% duty square wave was observed on the bridge output. 

![12 V PWM bridge output](Images/scope3.jpg)

There is some oscillation/ringing on the rising edge, but this is likely due to the fast switching time of the MOSFETs, and shouldn't affect our motor driving capability.
