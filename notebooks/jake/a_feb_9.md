# Activities Performed on February 9th, 2022

## Tilt Motor Torque Calculation

The monitor we have chosen weighs approximately 10 pounds, or 44.4 Newtons. If we assume a reasonable moment arm length of 0.1m, the gravitational torque on the motor will be ~4.5 Nm.
The tilt motor will have to be driven using a worm drive system, so as to prevent back-driving the motor with gravity.
This system will likely introduce a roughly 4:1 reduction in gearing, so our motor should be able to output ~1.2 Nm. 

For a good safety factor, and to account for frictional losses, our motor's stall torque should ideally be roughly 4 times this value.
This gives us an approximate motor torque requirement of 5 Nm. A gearmotor will have to be selected to fit this requirement within the current limit of 10 A continuous draw.
