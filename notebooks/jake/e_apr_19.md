# Activities Performed on April 19, 2022

## Control Loop Tuning

Today, I managed to successfully tune the onboard control loop to be able to drive each motor to a specified position.
My original plan was to use only P control, or proportional control. This means that the command to each motor is simply
(current position - target position) * P, where P is some constant. I initially thought that P would have to be fairly small,
so as to avoid overshooting the target and having to "bounce" around it. However, using a float value for P means that the 
STM32 has to do floating point math. As this microcontroller does not have dedicated floating point hardware, this process is 
extremely computationally costly. Because of the implementation of my motor driver function, this results in incorrect PWM generation 
due to high latency. 

To solve this problem, I reverted to only integer values of P. In fact, a P value of 1 worked extremely well for this application. 
This meant the motor command calculation was very simple. The command would often be in the thousands, but my motor driver function
clips excessive values to 17, or 85% duty cycle. I observed zero overshoot and rapid response from this control system. 

In order to debug this, I needed a way to observe the live encoder value. This was initially difficult without any printing ability, 
as this Cortex-M0 does not have a convenient way to print over SWD (Serial Wire Debug). However, I discovered that the STM32CubeIDE
has a "live expression" function in the debugger. This allows for live evaluation of any expression, using onboard variables and registers.
This allowed me to view the current calculated encoder value, which was practically required to solve this problem.
