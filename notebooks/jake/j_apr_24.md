# Activities Performed on April 24, 2022

## Adding Monitor Up/Down Adjust Capability

One of our core requirements is the ability to adjust the monitor's height with the wired remote control.
In order to do this, I simply added a few lines of code in the main microcontroller loop, which 
polled the values of the "monitor up" and "monitor down" buttons, and then moved the linear actuator accordingly.
The linear actuator was configured as our third motor driver, and so simply sending commands to driver 3 with our 
default MotorControl function was sufficient to drive it. 

```c
// Check to see if we're moving up or down

int command3 = (GPIOD->IDR & (MON_UP_Pin|MON_DOWN_Pin)) > 0;

MotorControl(abs(command1), command1<0, abs(command2), command2<0,
	command3*17, GPIOD->IDR & MON_UP_Pin);
```

This worked very well, and the up/down buttons were then able to be used to control the monitor's height.
The linear actuator was more than capable of handling the mechanism's weight, and rose and fell without 
issue.

## Fixing Control Loop Issue

After many hours of testing, I noticed that sometimes, the control loop wouldn't always drive the motors 
to the perfect place. Occasionally, the microcontroller would drive the motor to within 1 or 2 encoder ticks
of the target, and then stop. I determined that this was because the motor command was so low, it was not
strong enough to move the motor in some circumstances. To fix this, I added a "dead zone" around the target,
which would count as having reached it. 

```c
if (command1 < 5 && command2 < 5) {
	enc1_reading = 0;
	enc2_reading = 0;
	enc1_target = 0;
	enc2_target = 0;
}
```

The way in which our motor control code works is that once the target is reached, the target and current encoder positions
are zeroed. This means that any move is a relative move, and the absolute position of the encoders is not important. 
This was chosen because we had no way of zeroing the encoder positions with external limit switches, so the absolute
position was never known with any degree of precision upon startup. 

## Calibrating Encoder/Angle Conversion Factor

In order to properly drive the motors to an angle we desired, it was necessary to determine exactly how many 
encoder ticks represented a degree of motion. To do this, we commanded the pan motor to move exactly 10,000
ticks clockwise. Pictures were taken before and after this move with a static camera, so that image 
editing software could be used to determine the angle traveled. I used GIMP to do this function, and 
some screenshots of the process are included.

![angle 1](Images/angle1.jpg)

![angle 3](Images/angle3.jpg)

The calculated angle traveled was 23.03 degrees, in 10,000 encoder steps. Dividing these numbers, 
we arrived at a conversion factor of 434.216 encoder steps per degree. This value was then hardcoded
into the Pi's face detection code, and multiplied by any angle value to yield a number of encoder steps.
This encoder step value was then rounded off to the nearest integer, and transmitted over SPI to the STM32.
This resulted in a very accurate camera move.
