# Activities Performed on April 21, 2022

Today I resolved a major issue relating to encoder reading. I had noticed that occasionally, the motors would move uncommanded, 
or refuse to stop moving once the target position was reached. This was an intermittent problem, so it was somewhat difficult
to debug. However, I noticed that even when the motors were unplugged from the power supply, the encoder values would occasionally change.
This led me to suspect that noise was an issue in our circuit. Upon inspecting the encoder signals with an oscilloscope, I found that
there was significant noise on one of the encoder lines (ENC1\_A). 

As it turned out, the high switching currents in the motor driver circuit were injecting significant noise into the encoder lines. 
This was because one encoder line in particular had to be routed along a small part of the 12 V copper plane, which carried a significant
amount of current to the motors. This problem was solved by cutting this PCB trace, and soldering a jumper wire to bypass this noisy area.

![encoder jumper wire](Images/encoder_jumper.jpg)

Once this fix was put in place, I was unable to notice the same encoder issue. It appears that this has solved the problem.
